var express = require('express');
const bodyParser = require('body-parser');
var User = require('../models/user');
var router = express.Router();
var passport = require('passport');
var authenticate = require('../authenticate');
var cors = require('./cors');
const secret = require('../config').secretKey

/* GET users listing. */
router.options('*', cors.corsWithOptions, (req, res) => { res.sendStatus(200); });
router.get('/', cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/signup', cors.corsWithOptions, (req, res, next) => {
  User.register(new User({username: req.body.username}), 
    req.body.password, (err, user) => {
      if (err) {
        res.statusCode = 500;
        res.setHeader('Content-Type', 'application/json');
        res.json({err: err});
      }
      else {
        if (req.body.firstname) {
          user.firstname = req.body.firstname;
        }
        if (req.body.lastname) {
          user.lastname = req.body.lastname;
        }
        user.save((err, user) => {
          if (err){
            res.statusCode = 500;
            res.setHeader('Content-Type', 'application/json');
            res.json({err: err});
            return ;
          }
          passport.authenticate('local')(req, res, () => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json({success: true, status: 'Registration Successful!'})
          });
        });
    }
  });
});

router.post('/login', cors.corsWithOptions, (req, res, next) => {

  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err);
    }
    if(!user) {
      res.statusCode = 401;//unauthorized
      res.setHeader('Content-Type', 'application/json');
      res.json({success: false, status: 'Login Unsuccessful!', err: info})
    }
    req.logIn(user, (err) => {
      if (err) {
        res.statusCode = 401;//unauthorized
        res.setHeader('Content-Type', 'application/json');
        res.json({success: false, status: 'Login Unsuccessful!', err: 'Could not log in user'})
      }
    
      var token = authenticate.getToken({_id: req.user._id});
      res.statusCode = 200
      res.setHeader('Content-Type', 'application/json');
      res.json({success: true, status: 'Login Successful!', token: token})
    });
  }) (req, res, next);
});

router.get('/logout', cors.corsWithOptions, (req, res) => {
  if (req.session) {
    // this will remove the information in server
    req.session.destroy();
    //this will delete the cookie from the client side
    res.clearCookie('session-id');
    res.redirect('/');
  }
  else {
    var err = new Error('You are not logged in');
    err.status = 403; //forbiden operation
    next(err);
  }
});

router.get('/facebook/token', passport.authenticate('facebook-token'), (req, res) => {
  if (req.user) {
    var token = authenticate.getToken({_id: req.user._id});
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json({success: true, token: token, status: 'You are Successfully logged in!'})
  }
});

var crypto = require('crypto');

function base64decode(data) {
	 while (data.length % 4 !== 0){
                data += '=';
          }
  data = data.replace(/-/g, '+').replace(/_/g, '/');
  return new Buffer(data, 'base64').toString('utf-8');
}

function parseSignedRequest(signedRequest, secret) {
  var encoded_data = signedRequest.split('.', 2);// decode the data
  var sig = encoded_data[0];
  var json = base64decode(encoded_data[1]);
  var data = JSON.parse(json);
  if (!data.algorithm || data.algorithm.toUpperCase() != 'HMAC-SHA256') {
    err = new Error('Unknown algorithm: ' + data.algorithm + '. Expected HMAC-SHA256');
  }
  else {
    var expected_sig = crypto.createHmac('sha256', secret).
    update(encoded_data[1]).
    digest('base64').
    replace(/\+/g, '-').
    replace(/\//g, '_').
    replace('=', '');
    if (sig !== expected_sig) {
      err = new Error('Invalid signature: ' + sig + '. Expected ' + expected_sig);
    }
  }
  return {'data': data, 'err': err}
}

/*/
  data = {
          "oauth_token": "{user-access-token}",
          "algorithm": "HMAC-SHA256",
          "expires": 1291840400,
          "issued_at": 1291836800,
          "user_id": "218471"
          }
  I can find the procedure here:
  https://simran37delhi.medium.com/data-deletion-request-callback-javascript-9812a94829c3
/*/

router.post('/facebook-delete-user', cors.corsWithOptions, (req, res) => {
  response = parseSignedRequest(req, secret);
  if (!response['err']) {
    response_deletion = User.db.inventory.deleteOne(
      { "facebookID": response['data']['userID'] } // specifies the document to delete
    );
    if (response_deletion['acknowledged']) {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json({url: 'https://localhost:3443/', confirmation_code: 200})
    }
    else {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'application/json');
      res.json({url: 'https://localhost:3443/', confirmation_code: 404})
    }
  }
  else {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'application/json');
    res.json({url: 'https://localhost:3443/', confirmation_code: 404})
  }
});

//the token may be expired
router.get('/checkJWTToken', cors.corsWithOptions, (req, res) => {
  passport.authenticate('jwt', {session: false}, (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.statusCode = 401;
      res.setHeader('Content-Type', 'application/json');
      return res.json({status: 'JWT invalid', success: false, err: info})
    }
    else {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      return res.json({status: 'JWT valid', success: true, user: user})
    }
  }) (req, res);
})

module.exports = router;
